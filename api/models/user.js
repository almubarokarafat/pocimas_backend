const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const db = require("../connection/connection");

const userSchema = new mongoose.Schema(
  {
    nohp: { type: String, required: true, unique: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    role: { type: String, required: true },
    alamat: {
      lat: String,
      lon: String
    },
    saldo: { type: Number, default: 0 },
    merchantActive: Boolean,
    lastLogin: { type: Date, default: Date.now }
  },
  { versionKey: false }
);

userSchema.plugin(uniqueValidator, {
  message: "{VALUE} sudah terdaftar sebelumnya"
});

const User = () => {
  return db.useDb(process.env.DATABASE_NAME).model("Users", userSchema);
};

module.exports = User;
