const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const db = require("../connection/connection");

const orderSchema = new mongoose.Schema(
  {
    buyer: { type: mongoose.Schema.Types.ObjectId, ref: "Users" },
    seller: { type: mongoose.Schema.Types.ObjectId, ref: "Users" },
    totalHarga: Number,
    listItem: [
      {
        idProduk: { type: mongoose.Schema.Types.ObjectId, ref: "Produks" },
        title: String,
        harga: Number,
        qty: Number
      }
    ],
    orderedAt: { type: Date, default: Date.now },
    finishedAt: Date,
    status: { type: String, default: "cart" },
    alamat: {
      lat: String,
      lon: String
    }
  },
  { versionKey: false }
);

orderSchema.plugin(uniqueValidator, {
  message: "{VALUE} sudah terdaftar sebelumnya"
});

const Order = () => {
  return db.useDb(process.env.DATABASE_NAME).model("Orders", orderSchema);
};

module.exports = Order;
