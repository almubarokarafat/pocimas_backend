const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const db = require("../connection/connection");

const produkSchema = new mongoose.Schema(
  {
    title: { type: String, required: true },
    images: { type: String, default: "" },
    categories: [],
    dimensions: {
      length: Number,
      width: Number,
      height: Number,
      unit: String
    },
    weight: {
      value: Number,
      unit: String
    },
    stok: { type: Number, required: true },
    insertedAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
    insertedBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Users"
    },
    updatedBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Users"
    },
    harga: { type: Number, required: true }
  },
  { versionKey: false }
);

produkSchema.plugin(uniqueValidator, {
  message: "{PATH} yang anda masukkan sudah ada"
});

const Produk = () => {
  return db.useDb(process.env.DATABASE_NAME).model("Produks", produkSchema);
};

module.exports = Produk;
