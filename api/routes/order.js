const express = require("express");
const router = express.Router();
const OrderBuyerController = require("../controllers/OrderBuyerController");
const OrderSellerController = require("../controllers/OrderSellerController");

// Buyer
router.get("/beli", OrderBuyerController.getOrderBuyer);
router.get("/beli/:id", OrderBuyerController.getOrderBuyerById);
router.get("/beli/status/:status", OrderBuyerController.getOrderBuyerByStatus);

router.post("/beli", OrderBuyerController.postOrderBuyer);
router.post("/beli/langsung", OrderBuyerController.postOrderBayarLangsung);
router.post("/beli/bayar/:id", OrderBuyerController.postBuyerBayarOrder);
router.post("/beli/cancel/:id", OrderBuyerController.postCancelOrder);
router.post("/beli/receive/:id", OrderBuyerController.postFinishOrder);

// Seller
router.get("/jual", OrderSellerController.getOrderSeller);
router.get("/jual/:id", OrderSellerController.getOrderSellerById);
router.get(
  "/jual/status/:status",
  OrderSellerController.getOrderSellerByStatus
);

router.post("/jual/package/:id", OrderSellerController.postOrderSellerPackage);
router.post("/jual/send/:id", OrderSellerController.postOrderSellerSend);

module.exports = router;
