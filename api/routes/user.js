const { requiredJWT, merchantJWT } = require("../../middleware");
const express = require("express");
const router = express.Router();
const UserController = require("../controllers/UserController");

router.post("/register", UserController.postRegister);
router.post("/login", UserController.postLogin);
router.post("/facebook/page", UserController.facebookApiPostToPage)
router.post("/facebook/group", UserController.facebookApiPostToGroup)
router.post("/twitter", UserController.postTwitter)
router.post("/topup", requiredJWT, UserController.postTopUp);

router.get("/facebook/groups", UserController.facebookApiGetGroup)
router.get("/info", requiredJWT, UserController.getInfo);
router.get("/", requiredJWT, UserController.getAllProduk);

router.put("/alamat", requiredJWT, UserController.putAlamatUser);

module.exports = router;
