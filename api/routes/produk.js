const express = require("express");
const router = express.Router();
const ProdukController = require("../controllers/ProdukController");

router.get("/all", ProdukController.getAllProduk);
router.get("/:id", ProdukController.getProduk);
router.post("/", ProdukController.postProduk);
router.put("/:id", ProdukController.putProduk);
router.delete("/:id", ProdukController.deleteProduk);

module.exports = router;
