const mongoose = require("mongoose");

const db = mongoose.createConnection(process.env.DATABASE_CONNECTION_STRING, {
  useNewUrlParser: true,
  useFindAndModify: false,
  poolSize: 25
});

module.exports = db;
