const fs = require("fs");

const AWS = require("aws-sdk");
const s3 = new AWS.S3({
  accessKeyId: "<key>",
  secretAccessKey: "<key>"
});

const uploadS3 = async (filename, data) => {
  const params = {
    Bucket: "pocimas",
    Key: filename,
    Body: data
  };
  let result = "";
  s3.upload(params, (err, res) => {
    if (err) throw err;
    result = res.Location;
    return res.Location;
  });
  console.log("result", result);
  return result;
};

module.exports = { uploadS3 };

const doUpload = filepath => {
  fs.readFile(filepath, async (err, data) => {
    const res = uploadS3(filepath, data);
    console.log({ res });
  });
};

doUpload("35e1c916-3b02-4c93-bb13-8c499edaf38f.jpg");
