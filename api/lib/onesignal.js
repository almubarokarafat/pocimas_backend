const OneSignal = require("onesignal-node");

const sendNotif = (message, externalId) => {
  const client = new OneSignal.Client({
    userAuthKey: "<key>",
    // note that "app" must have "appAuthKey" and "appId" keys
    app: {
      appAuthKey: "<key>",
      appId: "<key>"
    }
  });

  var notif = new OneSignal.Notification({
    contents: {
      en: message
    },
    external_user_id: externalId,
    included_segments: ["Active Users", "Inactive Users"]
  });

  client.sendNotification(notif, (err, httpResponse, data) => {
    if (err) {
      console.log("Something went wrong...");
    } else {
      console.log(data, httpResponse.statusCode);
    }
  });
};

module.exports = { sendNotif };

sendNotif("Pesanan masuk, cek segera", "5d74032f82832cf676bde6c1");
