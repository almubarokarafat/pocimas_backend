const mongoose = require("mongoose");
const Produk = require("../models/produk");
const UploadS3 = require("../lib/uploadS3");
exports.getAllProduk = async (req, res, next) => {
  try {
    const { token } = req;
    const daftarProduk = await Produk().find({
      insertedBy: token.uid
    });
    res.status(200).json({
      code: 200200,
      payload: daftarProduk
    });
  } catch (error) {
    next(error);
  }
};

exports.getProduk = async (req, res, next) => {
  try {
    const { params } = req;
    const dataProduk = await Produk().findById(params.id);

    // produk tidak ada
    if (!dataProduk) {
      return res.status(404).json({
        code: 400404,
        message: "Produk tidak ditemukan"
      });
    }

    res.status(200).json({
      code: 200200,
      payload: dataProduk
    });
  } catch (error) {
    next(error);
  }
};

exports.postProduk = async (req, res, next) => {
  try {
    const { token, body } = req;

    const newProduk = await Produk().create({
      ...body,
      insertedBy: token.uid,
      updatedBy: token.uid
    });
    res.status(200).json({
      code: 200202,
      payload: newProduk
    });
  } catch (error) {
    next(error);
  }
};

exports.putProduk = async (req, res, next) => {
  try {
    const { token, body, params } = req;
    const updatedProduk = await Produk().findByIdAndUpdate(params.id, {
      ...body,
      updatedAt: new Date(),
      updatedBy: token.uid
    });

    // produk tidak ada
    if (!updatedProduk) {
      return res.status(404).json({
        code: 400404,
        message: "Produk tidak ditemukan"
      });
    }

    res.status(200).json({
      code: 200200,
      payload: "Produk berhasil di edit"
    });
  } catch (error) {
    next(error);
  }
};

exports.deleteProduk = async (req, res, next) => {
  try {
    const { params } = req;
    const deletedProduk = await Produk().findByIdAndRemove(params.id);

    // produk tidak ada
    if (!deletedProduk) {
      return res.status(404).json({
        code: 400404,
        message: "Produk tidak ditemukan"
      });
    }
    res.status(200).json({
      code: 200200,
      payload: "Produk berhasil di hapus"
    });
  } catch (error) {
    next(error);
  }
};
