const Order = require("../models/order");
const User = require("../models/user");
const Produk = require("../models/produk");
const { sendNotif } = require("../lib/onesignal");
exports.postOrderBuyer = async (req, res, next) => {
  try {
    const { token, body } = req;
    const { listItem, seller } = body;

    let totalHarga = 0;

    const user = await User()
      .findById(seller)
      .lean();

    // data tidak ditemukan
    if (!user) {
      return res.status(404).json({
        code: 400404,
        message: "Seller tidak ditemukan"
      });
    }

    const { alamat } = user;
    let newListItem = [];
    for (let order of listItem) {
      const { harga, qty, idProduk } = order;
      totalHarga += harga * qty;
      const produk = await Produk()
        .findById(idProduk)
        .lean();
      order.title = produk.title;
      newListItem.push(order);
    }
    body.totalHarga = totalHarga;
    body.alamat = alamat;
    body.listItem = newListItem;
    const insertedData = await Order().create({
      ...body,
      buyer: token.uid
    });

    res.status(200).json({
      code: 200200,
      payload: insertedData
    });
  } catch (error) {
    next(error);
  }
};

exports.getOrderBuyer = async (req, res, next) => {
  try {
    const { token } = req;
    const orderData = await Order().find({
      buyer: token.uid
    });
    res.status(200).json({
      code: 200200,
      payload: orderData
    });
  } catch (error) {
    next(error);
  }
};

exports.getOrderBuyerByStatus = async (req, res, next) => {
  try {
    const { token, params } = req;
    const data = await Order().find({
      buyer: token.uid,
      status: params.status
    });
    res.status(200).json({
      code: 200200,
      payload: data
    });
  } catch (error) {
    next(error);
  }
};

exports.getOrderBuyerById = async (req, res, next) => {
  try {
    const { params } = req;
    const data = await Order().findById(params.id);

    // data tidak ditemukan
    if (!data) {
      return res.status(404).json({
        code: 400404,
        message: "Order tidak ditemukan"
      });
    }
    res.status(200).json({
      code: 200200,
      payload: data
    });
  } catch (error) {
    next(error);
  }
};

exports.postBuyerBayarOrder = async (req, res, next) => {
  try {
    const { params, token } = req;
    const dataOrder = await Order()
      .findOne({
        _id: params.id,
        buyer: token.uid,
        status: "cart"
      })
      .lean();

    // data tidak ditemukan
    if (!dataOrder) {
      return res.status(404).json({
        code: 400404,
        message: "Order tidak ditemukan"
      });
    }
    const dataUser = await User()
      .findById(token.uid)
      .lean();

    const { saldo } = dataUser;
    const { listOrder, seller } = dataOrder;
    let totalBiaya = hitungTotalBiaya(listOrder);

    // cek keadaan saldo
    if (totalBiaya > saldo) {
      return res.status(409).json({
        code: 400409,
        message:
          "Saldo anda tidak cukup untuk melakukan pembayaran ini, silahkan isi saldo anda dahulu"
      });
    }

    // cek jumlah qty produk
    const dataProduks = await Produk()
      .find({
        insertedBy: seller._id
      })
      .lean();

    const result = cekKeadaanStok(listOrder, dataProduks);

    if (!result) {
      return res.status(409).json({
        code: 400409,
        message: "Stok tidak mencukupi"
      });
    }

    await Order().findByIdAndUpdate(params.id, {
      status: "ready"
    });

    await User().findByIdAndUpdate(token.uid, {
      $inc: {
        saldo: -totalBiaya
      }
    });

    await kurangiStok(listOrder);
    await sendNotif("Pesanan masuk, cek segera", seller);

    res.status(200).json({
      code: 200200,
      payload: "Berhasil melakukan pembayaran"
    });
  } catch (error) {
    next(error);
  }
};

exports.postOrderBayarLangsung = async (req, res, next) => {
  try {
    const { token, body } = req;

    const dataUser = await User()
      .findById(token.uid)
      .lean();

    const { saldo } = dataUser;
    const { listItem, seller } = body;
    let totalBiaya = hitungTotalBiaya(listItem);

    // cek keadaan saldo
    if (totalBiaya > saldo) {
      return res.status(409).json({
        code: 400409,
        message:
          "Saldo anda tidak cukup untuk melakukan pembayaran ini, silahkan isi saldo anda dahulu"
      });
    }

    // cek jumlah qty produk
    const dataProduks = await Produk()
      .find({
        insertedBy: seller._id
      })
      .lean();

    const result = cekKeadaanStok(listItem, dataProduks);

    if (!result) {
      return res.status(409).json({
        code: 400409,
        message: "Stok tidak mencukupi"
      });
    }

    const { alamat } = dataUser;
    let newListItem = [];
    for (let order of listItem) {
      const { harga, qty, idProduk } = order;
      totalHarga += harga * qty;
      const produk = await Produk()
        .findById(idProduk)
        .lean();
      order.title = produk.title;
      newListItem.push(order);
    }
    body.totalHarga = totalHarga;
    body.alamat = alamat;
    body.listItem = newListItem;

    const insertedData = await Order().create({
      ...body,
      buyer: token.uid,
      status: "ready"
    });

    await User().findByIdAndUpdate(token.uid, {
      $inc: {
        saldo: -totalBiaya
      }
    });

    await kurangiStok(listOrder);
    await sendNotif("Pesanan masuk, cek segera", seller);

    res.status(200).json({
      code: 200200,
      payload: "Berhasil melakukan order"
    });
  } catch (error) {
    next(error);
  }
};

exports.postCancelOrder = async (req, res, next) => {
  try {
    const { token, params } = req;
    const data = await Order().findOneAndUpdate(
      {
        _id: params.id,
        buyer: token.uid,
        status: "cart"
      },
      {
        status: "canceled",
        finishedAt: new Date()
      }
    );

    // data tidak ditemukan
    if (!data) {
      return res.status(404).json({
        code: 400404,
        message: "Order tidak ditemukan"
      });
    }

    res.status(200).json({
      code: 200200,
      payload: "Success cancel order"
    });
  } catch (error) {
    next(error);
  }
};

exports.postFinishOrder = async (req, res, next) => {
  try {
    const { token, params } = req;
    const data = await Order().findOneAndUpdate(
      {
        _id: params.id,
        buyer: token.uid,
        status: "sending"
      },
      {
        status: "received",
        finishedAt: new Date()
      }
    );

    // data tidak ditemukan
    if (!data) {
      return res.status(404).json({
        code: 400404,
        message: "Order tidak ditemukan"
      });
    }

    const { listOrder, seller } = data;
    let totalBiaya = hitungTotalBiaya(listOrder);

    await User().findByIdAndUpdate(seller._id, {
      $inc: {
        saldo: totalBiaya
      }
    });

    await sendNotif("Pesanan berhasil sampai", data.seller);

    res.status(200).json({
      code: 200200,
      payload: "Order diselesaikan"
    });
  } catch (error) {
    next(error);
  }
};

const hitungTotalBiaya = listOrder => {
  let totalBiaya = 0;
  for (order of listOrder) {
    const { harga, qty } = order;
    totalBiaya += harga * qty;
  }
  return totalBiaya;
};

const cekKeadaanStok = (listOrder, listProduk) => {
  const orderLength = listOrder.length;

  for (let i = 0; i < orderLength; i += 1) {
    const { qty, idProduk } = listOrder[i];
    const produkLength = listProduk.length;

    for (let j = 0; j < produkLength; j += 1) {
      const produk = listProduk[j];

      if (produk._id.toString() === idProduk.toString()) {
        listProduk[j].stok -= qty;
      }

      if (listProduk[j].stok < 0) {
        return false;
      }
    }
  }
  return true;
};

const kurangiStok = async listOrder => {
  const orderLength = listOrder.length;

  for (let i = 0; i < orderLength; i += 1) {
    const { qty, idProduk } = listOrder[i];

    await Produk().findByIdAndUpdate(idProduk, {
      $inc: {
        stok: -qty
      }
    });
  }
};
