const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const saltRounds = 10;
const config = require("../../config");
const axios = require("axios");

var request = require("request");
var twitterAPI = require("node-twitter-api");

const facebook_host = "https://graph.facebook.com/v4.0/";
const facebook_token = "";
const twitter_host = "https://api.twitter.com/1.1/";

const consumerKey = "";
const consumerSecret = "";
const requestToken = "";
const requestTokenSecret = "";
const accessToken = "";
const accessTokenSecret = "";

const User = require("../models/user");
const Produk = require("../models/produk");

// user register
exports.postRegister = async (req, res, next) => {
  try {
    const { nohp, email, password, role } = req.body;

    if (["ops", "merchant"].indexOf(role) < 0) {
      const err = new Error("Role tidak valid");
      err.code = 403;
      return next(err);
    }

    bcrypt.hash(password, saltRounds, async (err, hash) => {
      try {
        const savedUser = await User().create({
          nohp,
          email,
          password: hash,
          role
        });
        res.status(200).json({
          code: 200202,
          payload: savedUser
        });
      } catch (error) {
        // error validation
        error.code = 409;
        next(error);
      }
    });
  } catch (error) {
    next(error);
  }
};

// user login
exports.postLogin = async (req, res, next) => {
  try {
    const { nohp, email, password } = req.body;
    let userData = {};
    let loginWith = "";
    if (nohp) {
      // if user login with nohp
      loginWith = "Nomor Handphone";
      userData = await User()
        .findOne({ nohp })
        .lean();
    } else {
      // if user login with email
      loginWith = "Email";
      userData = await User()
        .findOne({ email })
        .lean();
    }

    // if user data not found, wrong nohp or email
    if (!userData) {
      return res.status(403).json({
        code: 400403,
        message: `${loginWith} atau Password anda salah`
      });
    }

    const passwordHash = userData.password;

    const match = await bcrypt.compare(password, passwordHash);
    if (match) {
      const token = jwt.sign(
        {
          role: userData.role,
          uid: userData._id.toString()
        },
        config.secret,
        { expiresIn: "1d" }
      );

      await User().findByIdAndUpdate(userData._id, {
        $set: {
          lastLogin: new Date()
        }
      });

      res.status(200).json({
        code: 200200,
        payload: userData,
        token
      });
    } else {
      // wrong password
      res.status(403).json({
        code: 400403,
        message: `${loginWith} atau Password anda salah`
      });
    }
  } catch (error) {
    next(error);
  }
};

exports.postTopUp = async (req, res, next) => {
  try {
    const { token, body } = req;
    const data = await User().findByIdAndUpdate(token.uid, {
      $inc: {
        saldo: body.amount
      }
    });
    res.status(200).json({
      code: 200200,
      payload: "Success top up"
    });
  } catch (error) {
    next(error);
  }
};

exports.getInfo = async (req, res, next) => {
  try {
    const { token } = req;
    const data = await User().findById(token.uid);

    // data tidak ditemukan
    if (!data) {
      return res.status(404).json({
        code: 400404,
        message: " tidak ditemukan"
      });
    }

    res.status(200).json({
      code: 200200,
      payload: data
    });
  } catch (error) {
    next(error);
  }
};

exports.getAllProduk = async (req, res, next) => {
  try {
    const { token } = req;
    const data = await Produk().find({
      insertedBy: {
        $ne: token.uid
      }
    });
    res.status(200).json({
      code: 200200,
      payload: data
    });
  } catch (error) {
    next(error);
  }
};

exports.putAlamatUser = async (req, res, next) => {
  try {
    const { body, token } = req;
    const { alamat } = body;

    // data tidak ditemukan
    if (!alamat) {
      return res.status(404).json({
        code: 400404,
        message: "Alamat tidak boleh kosong"
      });
    }

    const data = await User().findByIdAndUpdate(token.uid, {
      $set: {
        alamat
      }
    });
    res.status(200).json({
      code: 200200,
      payload: "Success update alamat"
    });
  } catch (error) {
    next(error);
  }
};

exports.facebookApiGetGroup = async (req, res, next) => {
  try {
    var config = {
      headers: {
        Authorization: "Bearer " + facebook_token
      }
    };

    const result = await axios.get(facebook_host + "me?fields=groups", config);
    res.status(200).json({
      data: result.data.groups.data
    });
  } catch (error) {
    next(error);
  }
};

exports.facebookApiPostToPage = async (req, res, next) => {
  try {
    const { url, caption } = req.body;

    // hit facebook api to get page data
    const result = await axios.get(
      facebook_host + "me/accounts?access_token=" + facebook_token
    );

    // use new token and page id for posting on page
    const new_token = result.data.data[0].access_token;
    const page_id = result.data.data[0].id;

    var data = {
      url: url,
      caption: caption
    };

    var config = {
      headers: {
        Authorization: "Bearer " + new_token
      }
    };
    const new_result = await axios.post(
      facebook_host + page_id.toString() + "/photos",
      data,
      config
    );

    res.status(200).json({
      data: new_result.data
    });
  } catch (error) {
    next(error);
  }
};

exports.facebookApiPostToGroup = async (req, res, next) => {
  try {
    const { url, caption, id_group } = req.body;
    var config = {
      headers: {
        Authorization: "Bearer " + facebook_token
      }
    };

    var data = {
      url: url,
      caption: caption
    };

    const new_result = await axios.post(
      facebook_host + id_group.toString() + "/photos",
      data,
      config
    );

    res.status(200).json({
      data: new_result.data
    });
  } catch (error) {
    next(error);
  }
};

exports.postTwitter = async (req, res, next) => {
  try {
    const { media, isBase64, status } = req.body;
    var twitter = new twitterAPI({
      consumerKey,
      consumerSecret
    });

    await twitter.getRequestToken(function(
      error,
      requestToken,
      requestTokenSecret,
      results
    ) {
      if (error) {
        console.log("Error getting OAuth request token : ");
        console.log(error);
      } else {
        requestToken = requestToken;
        requestTokenSecret = requestTokenSecret;
        console.log(requestToken, requestTokenSecret, results);
      }
    });

    await twitter.getAccessToken(
      requestToken,
      requestTokenSecret,
      requestToken,
      function(error, accessToken, accessTokenSecret, results) {
        if (error) {
          console.log("get accesstoken");
          console.log(error);
        } else {
          //store accessToken and accessTokenSecret somewhere (associated to the user)
          //Step 4: Verify Credentials belongs here
          accessToken = accessToken;
          accessTokenSecret = accessTokenSecret;
        }
      }
    );

    await twitter.uploadMedia(
      {
        media: media,
        isBase64: isBase64
      },
      accessToken,
      accessTokenSecret,
      async (error, response) => {
        if (error) {
          console.log("error : ", error);
        } else {
          // console.log("response : ", response);
          await twitter.statuses(
            "update",
            {
              status: status,
              media_ids: response.media_id_string
            },
            accessToken,
            accessTokenSecret,
            function(error, data, response) {
              console.log(response);
              if (error) {
                res.status(422).json({
                  data: error
                });
              } else {
                res.status(200).json({
                  data: { data }
                });
              }
            }
          );
          // return response
        }
      }
    );
    // console.log("image : ", image)
    // const data = await twitter.statuses(
    //     "update",
    //     {
    //       status: status,
    //       media_id  : image.media_id
    //     },
    //     accessToken,
    //     accessTokenSecret,
    //     function(error, data, response) {
    //       if (error) {
    //           res.status(422).json({
    //               data : error
    //           })
    //       } else {
    //         res.status(200).json({
    //               data : { data }
    //           })
    //       }
    //     }
    // );
  } catch (error) {
    next(error);
  }
};
