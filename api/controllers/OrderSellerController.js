const { sendNotif } = require("../lib/onesignal");
const User = require("../models/user");
const Order = require("../models/order");

exports.getOrderSeller = async (req, res, next) => {
  try {
    const { token } = req;
    const data = await Order().find({
      seller: token.uid
    });
    res.status(200).json({
      code: 200200,
      payload: data
    });
  } catch (error) {
    next(error);
  }
};

exports.getOrderSellerByStatus = async (req, res, next) => {
  try {
    const { token, params } = req;
    const data = await Order().find({
      seller: token.uid,
      status: params.status
    });
    res.status(200).json({
      code: 200200,
      payload: data
    });
  } catch (error) {
    next(error);
  }
};

exports.getOrderSellerById = async (req, res, next) => {
  try {
    const { params } = req;
    const data = await Order().findById(params.id);

    // data tidak ditemukan
    if (!data) {
      return res.status(404).json({
        code: 400404,
        message: "Order tidak ditemukan"
      });
    }

    res.status(200).json({
      code: 200200,
      payload: data
    });
  } catch (error) {
    next(error);
  }
};

exports.postOrderSellerPackage = async (req, res, next) => {
  try {
    const { params, token } = req;
    const data = await Order()
      .findOneAndUpdate(
        {
          _id: params.id,
          seller: token.uid,
          status: "ready"
        },
        {
          status: "packaged"
        }
      )
      .lean();

    // data tidak ditemukan
    if (!data) {
      return res.status(404).json({
        code: 400404,
        message: "Order tidak ditemukan"
      });
    }
    await sendNotif("Pesanan anda sudah di kemas", data.buyer);

    res.status(200).json({
      code: 200200,
      payload: "Order berhasil di package"
    });
  } catch (error) {
    next(error);
  }
};

exports.postOrderSellerSend = async (req, res, next) => {
  try {
    const { params, token } = req;
    const data = await Order().findOneAndUpdate(
      {
        _id: params.id,
        seller: token.uid,
        status: "packaged"
      },
      {
        status: "sending"
      }
    );

    // data tidak ditemukan
    if (!data) {
      return res.status(404).json({
        code: 400404,
        message: "Order tidak ditemukan"
      });
    }
    await sendNotif("Pesanan anda sedang dikirim", data.buyer);

    res.status(200).json({
      code: 200200,
      payload: "Order berhasil di kirim"
    });
  } catch (error) {
    next(error);
  }
};
