/* eslint-disable no-console */
/* eslint-disable no-undef */
const Corporate = require("../api/models/corporate");
const db = require("../api/connection/connection");

// Initialize database for testing (Resetting database)
before(async () => {
  try {
    // Find & delete tester account if exist
    const corporate = await Corporate("root").findOneAndDelete({
      alias: "testercorp"
    });
    if (corporate._id) {
      await db.useDb(corporate._id.toString()).dropDatabase();
    }
    console.log("Database initialize success!");
  } catch (error) {
    console.log("Database initialize error!");
    console.log(error);
  }
});
