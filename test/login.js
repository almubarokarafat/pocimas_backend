// Import the dependencies for testing
import chai from "chai";
import chaiHttp from "chai-http";
import app from "../server";
// Configure chai
chai.use(chaiHttp);
chai.should();

describe("Login", () => {
  let corporateID = "";
  describe("Register Corporate /", () => {
    // Test to register corporate
    it("should create new corporate", done => {
      chai
        .request(app)
        .post(`/register`)
        .send({
          name: "Test Corporate",
          alias: "testercorp",
          username: "tester1234567",
          password: "123456"
        })
        .end((err, res) => {
          const { message } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          message.savedCorporate._id.should.be.a("string");
          corporateID = message.savedCorporate._id;
          console.log("Corporate with ID : ", corporateID, " has been created");
          done();
        });
    });
  });

  describe("GET /", () => {
    // Test to register corporate
    it("should get data", done => {
      chai
        .request(app)
        .get(`/testercorp/login`)
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
  });

  describe("GET /", () => {
    // Test to register corporate
    it("should get data", done => {
      chai
        .request(app)
        .get(`/testercorp/login/a`)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });

  describe("POST /", () => {
    it("should validate account", done => {
      chai
        .request(app)
        .post(`/${corporateID}/login`)
        .send({
          username: "tester1234567",
          password: "123456"
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          done();
        });
    });
  });

  describe("POST /", () => {
    // Test to register corporate
    it("should return wrong pass", done => {
      chai
        .request(app)
        .post(`/${corporateID}/login`)
        .send({
          username: "tester1234567",
          password: "12345"
        })
        .end((err, res) => {
          res.should.have.status(401);
          res.body.should.be.a("object");
          done();
        });
    });
  });

  describe("Delete Corporate /", () => {
    // Test to drop database & delete corporate test
    it("should drop database & delete corporate test", done => {
      console.log("corporate ID to delete :", corporateID);
      chai
        .request(app)
        .post(`/${corporateID}/drop-database`)
        .end((err, res) => {
          const { message } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          message.should.equal("success");
          done();
        });
    });
  });
});
