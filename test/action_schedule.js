// Import the dependencies for testing
import chai from "chai";
import chaiHttp from "chai-http";
import app from "../server";
// Configure chai
chai.use(chaiHttp);
chai.should();

describe.only("Action Schedule", function () {
  let corporateID = "";
  let scheduleID = "";
  describe("Register Corporate /", () => {
    // Test to register corporate
    it("should create new corporate", done => {
      chai
        .request(app)
        .post(`/register`)
        .send({
          name: "Test Corporate",
          alias: "testercorp",
          username: "tester1234567",
          password: "123456"
        })
        .end((err, res) => {
          const { message } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          message.savedCorporate._id.should.be.a("string");
          corporateID = message.savedCorporate._id;
          console.log("Corporate with ID : ", corporateID, " has been created");
          done();
        });
    });
  });

  describe("POST LOGIN /", () => {
    it("should validate account", done => {
      chai
        .request(app)
        .post(`/${corporateID}/login`)
        .send({
          username: "tester1234567",
          password: "123456"
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          done();
        });
    });
  });

  describe("POST /", () => {
    it("should be post new action schedule (daily)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2020-01-01",
          date_end: "2020-12-01",
          time_start: "00:00:00",
          time_end: "23:59:59",
          repeat: "Daily",
          day: null,
          day_num: null,
          time: "12:00:00"
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          scheduleID = payload._id;
          payload.nextSchedule.should.equal("2020-01-01T05:00:00.000Z");
          done();
        })
    })

    it("should be post new action schedule (kelewat hari)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2020-01-31",
          date_end: "2020-12-01",
          time_start: "00:00:00",
          time_end: "23:59:59",
          repeat: "Daily",
          day: null,
          day_num: null,
          time: "12:00:00"
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.nextSchedule.should.equal("2020-01-31T05:00:00.000Z");
          done();
        })
    })

    it("should be post new action schedule (date start < now)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2018-01-01",
          date_end: "2020-12-01",
          time_start: "00:00:00",
          time_end: "23:59:59",
          repeat: "Daily",
          day: null,
          day_num: null,
          time: "12:00:00"
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.nextSchedule.should.equal("2019-09-04T05:00:00.000Z");
          done();
        })
    })

    it("should be no next schedule (date_end < next_schedule)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2018-01-01",
          date_end: "2018-12-01",
          time_start: "00:00:00",
          time_end: "23:59:59",
          repeat: "Daily",
          day: null,
          day_num: null,
          time: "12:00:00"
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.should.equal("No next schedule");
          done();
        })
    })

    it("should be post new action schedule (beda hari)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2020-01-31",
          date_end: "2020-12-01",
          time_start: "00:00:00",
          time_end: "23:59:59",
          repeat: "Daily",
          day: null,
          day_num: null,
          time: "04:00:00"
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.nextSchedule.should.equal("2020-01-30T21:00:00.000Z");
          done();
        })
    })

    it("should be post new action schedule (weekly monday)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2020-02-03",
          date_end: "2020-12-01",
          time_start: "00:00:00",
          time_end: "23:59:59",
          repeat: "Weekly",
          day: "Monday",
          day_num: null,
          time: "12:00:00"
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.nextSchedule.should.equal("2020-02-03T05:00:00.000Z");
          done();
        })
    })

    it("should be post new action schedule (weekly wednesday)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2020-02-03",
          date_end: "2020-12-01",
          time_start: "00:00:00",
          time_end: "23:59:59",
          repeat: "Weekly",
          day: "Wednesday",
          day_num: null,
          time: "12:00:00"
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.nextSchedule.should.equal("2020-02-05T05:00:00.000Z");
          done();
        })
    })

    it("should be post new action schedule (weekly thursday)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2020-02-03",
          date_end: "2020-12-01",
          time_start: "00:00:00",
          time_end: "23:59:59",
          repeat: "Weekly",
          day: "Thursday",
          day_num: null,
          time: "12:00:00"
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.nextSchedule.should.equal("2020-02-06T05:00:00.000Z");
          done();
        })
    })

    it("should be post new action schedule (weekly friday)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2020-02-03",
          date_end: "2020-12-01",
          time_start: "00:00:00",
          time_end: "23:59:59",
          repeat: "Weekly",
          day: "Friday",
          day_num: null,
          time: "12:00:00"
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.nextSchedule.should.equal("2020-02-07T05:00:00.000Z");
          done();
        })
    })

    it("should be post new action schedule (weekly saturday)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2020-02-03",
          date_end: "2020-12-01",
          time_start: "00:00:00",
          time_end: "23:59:59",
          repeat: "Weekly",
          day: "Saturday",
          day_num: null,
          time: "12:00:00"
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.nextSchedule.should.equal("2020-02-08T05:00:00.000Z");
          done();
        })
    })

    it("should be post new action schedule (weekly sunday)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2020-02-03",
          date_end: "2020-12-01",
          time_start: "00:00:00",
          time_end: "23:59:59",
          repeat: "Weekly",
          day: "Sunday",
          day_num: null,
          time: "12:00:00"
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.nextSchedule.should.equal("2020-02-09T05:00:00.000Z");
          done();
        })
    })

    it("should be post new action schedule (weekly date_end)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2018-02-03",
          date_end: "2018-12-01",
          time_start: "00:00:00",
          time_end: "23:59:59",
          repeat: "Weekly",
          day: "Tuesday",
          day_num: null,
          time: "12:00:00"
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.should.equal("No next schedule");
          done();
        })
    })

    it("should be post new action schedule (weekly kelewat)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2020-02-03",
          date_end: "2020-12-01",
          time_start: "00:00:00",
          time_end: "23:59:59",
          repeat: "Weekly",
          day: "Monday",
          day_num: null,
          time: "12:00:00"
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.nextSchedule.should.equal("2020-02-03T05:00:00.000Z");
          done();
        })
    })

    it("should be post new action schedule (hourly)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2020-02-03",
          date_end: "2020-12-01",
          time_start: "16:00:00",
          time_end: "12:00:00",
          repeat: "Hourly",
          day: null,
          day_num: null,
          time: null
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.nextSchedule.should.equal("2020-02-03T09:00:00.000Z");
          done();
        })
    })

    it("should be post new action schedule (date_start < now)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2018-02-03",
          date_end: "2020-12-01",
          time_start: "16:00:00",
          time_end: "12:00:00",
          repeat: "Hourly",
          day: null,
          day_num: null,
          time: null
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          // payload.nextSchedule.should.equal("2019-09-03T09:00:00.000Z");
          done();
        })
    })

    it("should be post new action schedule (monthly)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2020-02-03",
          date_end: "2020-12-01",
          time_start: "00:00:00",
          time_end: "23:59:59",
          repeat: "Monthly",
          day: null,
          day_num: 10,
          time: "12:00:00"
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.nextSchedule.should.equal("2020-02-10T05:00:00.000Z");
          done();
        })
    })

    it("should be post new action schedule (monthly kelewat jam)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2020-02-10",
          date_end: "2020-12-01",
          time_start: "00:00:00",
          time_end: "23:59:59",
          repeat: "Monthly",
          day: null,
          day_num: 10,
          time: "12:00:00"
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.nextSchedule.should.equal("2020-03-10T05:00:00.000Z");
          done();
        })
    })

    it("should be post new action schedule (monthly kelewat hari)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2020-02-12",
          date_end: "2020-12-01",
          time_start: "00:00:00",
          time_end: "23:59:59",
          repeat: "Monthly",
          day: null,
          day_num: 10,
          time: "12:00:00"
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.nextSchedule.should.equal("2020-03-10T05:00:00.000Z");
          done();
        })
    })

    it("should be post new action schedule (monthly jumlah hari < day_num)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2020-02-10",
          date_end: "2020-12-01",
          time_start: "00:00:00",
          time_end: "23:59:59",
          repeat: "Monthly",
          day: null,
          day_num: 31,
          time: "12:00:00"
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.nextSchedule.should.equal("2020-02-29T05:00:00.000Z");
          done();
        })
    })

    it("should be post new action schedule (before last day of the month)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2020-02-10",
          date_end: "2020-12-01",
          time_start: "00:00:00",
          time_end: "23:59:59",
          repeat: "Last date of the month",
          day: null,
          day_num: null,
          time: "12:00:00"
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.nextSchedule.should.equal("2020-02-29T05:00:00.000Z");
          done();
        })
    })

    it("should be post new action schedule (last day of the month)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2020-03-31",
          date_end: "2020-12-01",
          time_start: "00:00:00",
          time_end: "23:59:59",
          repeat: "Last date of the month",
          day: null,
          day_num: null,
          time: "12:00:00"
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.nextSchedule.should.equal("2020-03-31T05:00:00.000Z");
          done();
        })
    })

    it("should be post new action schedule (this month)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2018-03-31",
          date_end: "2020-12-01",
          time_start: "00:00:00",
          time_end: "23:59:59",
          repeat: "Last date of the month",
          day: null,
          day_num: null,
          time: "12:00:00"
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.nextSchedule.should.equal("2019-09-30T05:00:00.000Z");
          done();
        })
    })

    it("should be post new action schedule (every weekday)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2020-02-10",
          date_end: "2020-12-01",
          time_start: "00:00:00",
          time_end: "23:59:59",
          repeat: 'Every Weekday',
          day: null,
          day_num: null,
          time: "12:00:00"
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.nextSchedule.should.equal("2020-02-10T05:00:00.000Z");
          done();
        })
    })

    it("should be post new action schedule (every weekday from saturday)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2020-02-08",
          date_end: "2020-12-01",
          time_start: "00:00:00",
          time_end: "23:59:59",
          repeat: 'Every Weekday',
          day: null,
          day_num: null,
          time: "12:00:00"
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.nextSchedule.should.equal("2020-02-10T05:00:00.000Z");
          done();
        })
    })

    it("should be post new action schedule (every weekday - Sunday)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2020-02-09",
          date_end: "2020-12-01",
          time_start: "00:00:00",
          time_end: "23:59:59",
          repeat: 'Every Weekday',
          day: null,
          day_num: null,
          time: "12:00:00"
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.nextSchedule.should.equal("2020-02-10T05:00:00.000Z");
          done();
        })
    })

    it("should be post new action schedule (every weekday - friday)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2020-02-07",
          date_end: "2020-12-01",
          time_start: "00:00:00",
          time_end: "23:59:59",
          repeat: 'Every Weekday',
          day: null,
          day_num: null,
          time: "12:00:00"
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.nextSchedule.should.equal("2020-02-07T05:00:00.000Z");
          done();
        })
    })

    it("should be no next action schedule (no repeat)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2020-02-07",
          date_end: "2020-12-01",
          time_start: "00:00:00",
          time_end: "23:59:59",
          repeat: '',
          day: null,
          day_num: null,
          time: "12:00:00"
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.should.equal("No next schedule");
          done();
        })
    })
    it("should be post new action schedule (one time)", done => {
      chai
        .request(app)
        .post(`/${corporateID}/action-schedule`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          date_start: "2020-02-07",
          date_end: null,
          time_start: "10:00:00",
          time_end: null,
          repeat: 'Does not repeat',
          day: null,
          day_num: null,
          time: null
        })
        .end((err, res) => {
          const { payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.nextSchedule.should.equal("2020-02-07T03:00:00.000Z");
          done();
        })
    })
  })

  describe("GET /", () => {
    it("should be get all action schedule", done => {
      chai
        .request(app)
        .get(`/${corporateID}/action-schedule`)
        .end((err, res) => {
          const { code } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          code.should.equal(200);
          done();
        })
    })

    it("should be get schedule by ID", done => {
      chai
        .request(app)
        .get(`/${corporateID}/action-schedule/${scheduleID}`)
        .end((err, res) => {
          const { code, payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.nextSchedule.should.equal("2020-01-01T05:00:00.000Z");
          code.should.equal(200);
          done();
        })
    })
  });

  describe("PUT /", () => {
    it("should be get schedule by ID", done => {
      chai
        .request(app)
        .put(`/${corporateID}/action-schedule/${scheduleID}`)
        .send({
          actionConfigID: "5djasmosnsmcps",
          conditionID: "5d6e32a9f5c877396a6d22aa",
          thenID: "5d6e32a9f5c877396a6d22ab",
          nextSchedule: "2020-01-02T05:00:00.000Z"
        })
        .end((err, res) => {
          const { code, payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload.nextSchedule.should.equal("2020-01-02T05:00:00.000Z");
          code.should.equal(200);
          done();
        })
    })
  });

  describe("DELETE /", () => {
    it("should be delete action schedule by ID", done => {
      chai
        .request(app)
        .delete(`/${corporateID}/action-schedule/${scheduleID}`)
        .end((err, res) => {
          const { code, payload } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          payload._id.should.equal(scheduleID);
          code.should.equal(200);
          done();
        })
    })
  });

  describe("Delete Corporate /", () => {
    // Test to drop database & delete corporate test
    it("should drop database & delete corporate test", done => {
      console.log("corporate ID to delete :", corporateID);
      chai
        .request(app)
        .post(`/${corporateID}/drop-database`)
        .end((err, res) => {
          const { message } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          message.should.equal("success");
          done();
        });
    });
  });
});
