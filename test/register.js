// Import the dependencies for testing
import chai from "chai";
import chaiHttp from "chai-http";
import app from "../server";
// Configure chai
chai.use(chaiHttp);
chai.should();
let corporateID = "";


describe("Register", () => {
  let corporateID = "";
  describe("POST /", () => {
    // Test to register corporate
    it("should create new corporate", done => {
      chai
        .request(app)
        .post(`/register`)
        .send({
          name: "Test Corporate",
          alias: "testercorp",
          username: "tester1234567",
          password: "123456"
        })
        .end((err, res) => {
          const { message } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          message.savedCorporate._id.should.be.a("string");
          corporateID = message.savedCorporate._id;
          console.log("Corporate with ID : ", corporateID, " has been created");
          done();
        });
    });

    // Test insert duplicate corporate alias should be return 400 FAIL
    it("should return error duplicated corporate alias", done => {
      chai
        .request(app)
        .post(`/register`)
        .send({
          name: "Test Corporate",
          alias: "testercorp",
          username: "tester1234567",
          password: "123456"
        })
        .end((err, res) => {
          const { message } = res.body;
          res.should.have.status(400);
          res.body.should.be.a("object");
          done();
        });
    });
  });

  describe("Drop Created Corporate /", () => {
    // Test to drop database & delete corporate test
    it("should drop database & delete corporate test", done => {
      console.log("corporate ID to delete :", corporateID);
      chai
        .request(app)
        .post(`/${corporateID}/drop-database`)
        .end((err, res) => {
          const { message } = res.body;
          res.should.have.status(200);
          res.body.should.be.a("object");
          message.should.equal("success");
          done();
        });
    });
  });
});
