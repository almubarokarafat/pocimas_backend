const express = require("express");
const morgan = require("morgan");
var cors = require("cors");
const bodyParser = require("body-parser");
const swaggerUi = require("swagger-ui-express");
const specs = require("./swagger-doc");
const multer = require("multer");
const formData = require("express-form-data");
const { requiredJWT, opsJWT, merchantJWT } = require("./middleware");
// Routes
const HomeRoute = require("./api/routes/home");
const UsersRoute = require("./api/routes/user");
const ProdukRoute = require("./api/routes/produk");
const OrderRoute = require("./api/routes/order");
const app = express();

// Add cors setting
app.use(cors());

app.use(morgan("dev"));

// Add body parser
app.use(bodyParser.urlencoded({ extended: false, limit: "1000mb" }));
app.use(bodyParser.json({ limit: "1000mb" }));
app.use(multer().array("images", 30));
// app.use(formData.union());

// Routes
app.use("/", HomeRoute);
app.use("/users", UsersRoute);

// merchant side
app.use("/produk", requiredJWT, merchantJWT, ProdukRoute);
app.use("/order", requiredJWT, merchantJWT, OrderRoute);

// Swagger
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(specs));

// Handling error response 404 route not found
app.use((req, res, next) => {
  const error = new Error("Route tidak diketahui");
  error.code = 404;
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.code || 500).json({
    code: error.code || 500,
    message: error.message
  });
});

module.exports = app;
