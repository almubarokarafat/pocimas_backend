let jwt = require("jsonwebtoken");
const config = require("./config.js");

const requiredJWT = (req, res, next) => {
  let token = req.headers["x-access-token"] || req.headers["authorization"]; // Express headers are auto converted to lowercase
  if (token) {
    if (token.startsWith("Bearer ")) {
      // Remove Bearer from string
      token = token.slice(7, token.length);
    }
    jwt.verify(token, config.secret, (err, decoded) => {
      if (err) {
        return res.status(401).json({
          code: 400401,
          message: err.message
        });
      } else {
        req.token = decoded;
        next();
      }
    });
  } else {
    // token not found
    return res.status(403).json({
      code: 400403,
      message: "Auth Token tidak di temukan"
    });
  }
};

const opsJWT = (req, res, next) => {
  const { token } = req;
  if (token.role === "ops") {
    next();
  } else {
    return res.status(403).json({
      code: 400403,
      message: "Anda tidak diijinkan mengakses halaman ini"
    });
  }
};

const merchantJWT = (req, res, next) => {
  const { token } = req;
  if (token.role === "merchant") {
    next();
  } else {
    return res.status(403).json({
      code: 400403,
      message: "Anda tidak diijinkan mengakses halaman ini"
    });
  }
};

module.exports = { requiredJWT, opsJWT, merchantJWT };
